package vn.edu.vnuk.record.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import vn.edu.vnuk.record.jdbc.ConnectionFactory;
import vn.edu.vnuk.record.model.Contact;

public class ContactDao {
	
	private Connection connection;
	
	
	public ContactDao() {
		this.connection = new ConnectionFactory().getConnection();
	}
	
	public ContactDao(Connection connection) {
		this.connection = connection;
	}
	
	public void create(Contact contact) throws SQLException {
		String sqlQuery = "insert into contacts (name,email,address,date_of_birth) "
				+ "values (?,?,?,?) ";


		PreparedStatement statement;
		
		try {
			statement = connection.prepareStatement(sqlQuery);
			statement.setString(1, contact.getName());
			statement.setString(2, contact.getEmail() );
			statement.setString(3, contact.getAddress());
			
			statement.setDate(
				4, 
				new java.sql.Date(
					contact.getDateOfBirth().getTimeInMillis()
				)
			);
			statement.execute();
			statement.close();
			System.out.println("New record in DB");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finally {
			connection.close();
			System.out.println("Done!");
		}
			}
	
	//read(list of contacts)
	@SuppressWarnings("finally")
	public List<Contact> read() throws SQLException {
		
		String sqlQuery = "select * from contacts";
		
		List<Contact> contacts = new ArrayList<Contact>();

		PreparedStatement statement;
		
		try {
			statement = connection.prepareStatement(sqlQuery);
			
			ResultSet results = statement.executeQuery();
				while(results.next()) {
					contacts.add(buildContact(results));
				}
			results.close();
			statement.close();
			System.out.println("New record in DB");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finally {
			return contacts;
		}
	}
		
		@SuppressWarnings("finally")
		public Contact read(long id) throws SQLException {
			
			String sqlQuery = "select * from contacts where id ='" + id + "'";
			
			Contact contact = new Contact();
			
			PreparedStatement statement;
			
			try {
				statement = connection.prepareStatement(sqlQuery);
				
				ResultSet results = statement.executeQuery();
					if(results.next()) {
						
						contact = buildContact(results);
						
					}
				results.close();
				statement.close();
				System.out.println("New record in DB");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			finally {
				return contact;
			}
			
		
		}
		
		public void update(long id) throws SQLException {
			String sqlQuery = "UPDATE contacts SET address = '41blo' WHERE id = '?'" ;
			PreparedStatement statement;
			
			try {
				statement = connection.prepareStatement(sqlQuery);
				
				statement.execute();
				statement.close();
				System.out.println("Successfully updated!");
			} catch (SQLException e) {
				e.printStackTrace();
			} 
		}
		
		public void destroy(long id) throws SQLException {
			String sqlQuery = "DELETE FROM contacts WHERE id = '?'" ;
			PreparedStatement statement;
			
			try {
				statement = connection.prepareStatement(sqlQuery);
				
				statement.execute();
				statement.close();
				System.out.println("Successfully destroyed!");
			} catch (SQLException e) {
				e.printStackTrace();
			} 
		}
		
		
		private Contact buildContact(ResultSet results) throws SQLException {
			
			Contact contact = new Contact();
			contact.setId(results.getLong("id"));
			contact.setName(results.getString("name"));
			contact.setEmail(results.getString("email"));
			contact.setAddress(results.getString("address"));
			
			Calendar date = Calendar.getInstance();
			date.setTime(results.getDate("date_of_birth"));
			contact.setDateOfBirth(date);
			
			return contact;
			
		}

	
}
