package vn.edu.vnuk.record.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

@WebFilter("/*")
public class RunningTimeFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		//before doFilter
		long initialTime = System.currentTimeMillis();	
		
		chain.doFilter(request, response);
		
		//after doFilter	
		long finalTime = System.currentTimeMillis();
		
		String uri = ((HttpServletRequest)request).getRequestURI();
		String parameter = ((HttpServletRequest)request).getParameter("logic");
		System.out.println("Running " + uri + "?logic=" + parameter + " took "
							+ (finalTime- initialTime ) + " ms");
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}
	
	@Override
	public void destroy() {
		
	}

}
