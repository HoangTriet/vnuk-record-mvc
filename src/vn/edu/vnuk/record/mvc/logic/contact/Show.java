package vn.edu.vnuk.record.mvc.logic.contact;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.edu.vnuk.record.dao.ContactDao;
import vn.edu.vnuk.record.model.Contact;
import vn.edu.vnuk.record.mvc.logic.Logic;

public class Show implements Logic {

	@Override
	public String run(HttpServletRequest request, HttpServletResponse respone) throws Exception {
		Connection connection = (Connection) request.getAttribute("myConnection");
		long id = Long.parseLong(request.getParameter("id"));
		
		request.setAttribute("contact", new ContactDao(connection).read(id));
		
		
		return "/WEB-INF/jsp/contact/show.jsp";
	}
}
