package vn.edu.vnuk.record.mvc.logic.contact;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.edu.vnuk.record.dao.ContactDao;
import vn.edu.vnuk.record.model.Contact;
import vn.edu.vnuk.record.mvc.logic.Logic;

public class Create implements Logic {

	@Override
	public String run(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		System.out.println("Adding contact...");
		return "/WEB-INF/jsp/contact/create.jsp";
	}
	

}