package vn.edu.vnuk.record.mvc.logic;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FirstLogic implements Logic {

	@Override
	public String run(HttpServletRequest request, HttpServletResponse respone) throws Exception {
		// TODO Auto-generated method stub
			System.out.println("Running Logic and redirecting");
			
		return "first-logic.jsp";
	}

}
