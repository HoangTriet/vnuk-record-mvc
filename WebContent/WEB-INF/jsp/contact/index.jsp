
<html>

	<head>
		<link href="css/bootstrap.min.css" type="text/css" rel="stylesheet" />
	</head>
	
	<body>
		<%@taglib uri ="http://java.sun.com/jsp/jstl/core" prefix="c" %>
		<%@taglib uri ="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
		
		<c:import url="../header.jsp"></c:import>
		
		<table>
			<c:forEach var= "contact" items= "${contacts}">
					  
			<tr>
					<td>${contact.name}</td>
					<td>
						<c:choose>
							<c:when test="${not empty contact.email }">
								<a href="mailto:${contact.email }"> ${contact.email} </a>
							</c:when>
							
							<c:otherwise>
								<i>No email address</i>
							</c:otherwise>
						</c:choose>
					</td>
					<td>${contact.address}</td>
					<td>
						<fmt:formatDate value="${contact.dateOfBirth.time }" pattern="dd/MM/yyyy"/>
					</td>
			</tr>
			
			<tr>
				<td>
					<a href="mvc?logic=contact.Delete&id=${contact.id}">
						<button type="button" class="btn btn-danger">Deleted</button>
					</a>
				</td>
			</tr>
			
			<tr>
				<td>
					<a href="mvc?logic=contact.Update&id=${contact.id}">Update</a>
				</td>
			</tr>
			
			<tr>
				<td>
					<a href="mvc?logic=contact.Show&id=${contact.id}">
						<button type="button" class="btn btn-basic">Show</button>
					</a>
				</td>
			</tr>
				
			</c:forEach>
			<tr>
				<td>
					<a href="mvc?logic=contact.Create=${contact.id}">
						<button type="button" class="btn btn-primary">Add new contact</button>
					</a>
				</td>
			</tr>
			
		</table>
		<c:import url="../footer.jsp"></c:import>
	</body>
</html>